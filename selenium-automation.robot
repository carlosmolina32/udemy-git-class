*** Settings ***
Library     String
Library     SeleniumLibrary
Documentation       el arrayYY lo que me va a permitir es hacer click en cada recuadro y luego en el logo de la pagina repetidamente

*** Variables ***
${browser}      chrome
${homepage}     automationpractice.com/index.php
${scheme}       http
${testUrl}      ${scheme}://${homepage}

*** Keywords ***

Open Homepage
    Open Browser    ${testUrl}      ${browser}

*** Test Cases ***
C001 Hacer Click en Contenedores
    Open Homepage
    Set Global Variable    @{nombresDeContenedores}      //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${nombresDeContenedor}     IN       @{nombresDeContenedores}
    \   Run Keyword If      '${nombresDeContenedor}' =='//*[@id="homefeatured"]/li[8]/div/div[2]/h5/a'    Exit For Loop
    \   Click Element       xpath=${nombresDeContenedor}
    \   Wait Until Element Is Visible   xpath=//*[@id="bigpic"]
    \   Click Element       xpath=//*[@id="header_logo"]/a/img
    Close Browser

C002 Caso de Prueba Nuevo
    Open.Homepage